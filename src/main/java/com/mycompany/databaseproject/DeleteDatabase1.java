/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.databaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Windows10
 */
public class UpdateDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:D-Coffee.db";
        //ConnectDatabase
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been estalish.");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        String sql = "DELETE FROM CATEGORY WHERE CATEGORY_ID=? ";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 5);
            
            
            int status = stmt.executeUpdate();
            //ResultSet key = stmt.getGeneratedKeys(); 
            //key.next();
            //System.out.println("" + key.getInt(1));
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }

        //clse Database 
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
